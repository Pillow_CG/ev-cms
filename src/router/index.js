import Vue from 'vue'
import VueRouter from 'vue-router'
import Reg from '@/views/Reg/Reg.vue'
import Login from '@/views/Login/Login.vue'
import Main from '@/views/Main/Main.vue'
import Home from '@/views/Menus/Home/Home.vue'
import UserInfo from '@/views/Menus/User/UserInfo.vue'
import UserAvatar from '@/views/Menus/User/UserAvatar.vue'
import UserPwd from '@/views/Menus/User/UserPwd.vue'
import ArtCate from '@/views/Menus/Article/ArtCate.vue'
import ArtList from '@/views/Menus/Article/ArtList.vue'

Vue.use(VueRouter)

// 处理vue-router 重复调用 push 函数跳转到同一个路由地址时，报错的问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {
    name: 'Main',
    path: '/',
    component: Main,
    redirect: '/home',
    children:
      [
        {
          name: 'Home',
          path: 'Home',
          component: Home
        },
        {
          name: 'UserInfo',
          path: 'User-Info',
          component: UserInfo
        },
        {
          name: 'UserAvatar',
          path: 'User-Avatar',
          component: UserAvatar
        },
        {
          name: 'UserPwd',
          path: 'User-Pwd',
          component: UserPwd
        },
        {
          name: 'ArtCate',
          path: 'art-cate',
          component: ArtCate
        },
        {
          name: 'ArtList',
          path: 'art-list',
          component: ArtList
        }
      ]
  },
  {
    name: 'Login',
    path: '/Login',
    component: Login
  },
  {
    name: 'Reg',
    path: '/Reg',
    component: Reg
  }
]

const router = new VueRouter({
  routes
})

export default router
