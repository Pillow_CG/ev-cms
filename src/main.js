import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/global.less'

// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// 导入富文本编辑器的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 1、在main.js里面引入axios可以全组件使用
import axios from 'axios'

// 全局注册富文本编辑器
Vue.use(VueQuillEditor)

// 2、可以设置同一基地址
axios.defaults.baseURL = 'http://big-event-vue-api-t.itheima.net'
// 3、将axios设置为Vue的原型对象的属性
Vue.prototype.$axios = axios
// 4、后面所有使用axios的地方全部替换成 this.$axios

// 添加请求拦截器
axios.interceptors.request.use(
  function (config) {
    const token = store.state.token
    if (token && config.url.startsWith('/my')) {
      config.headers.Authorization = token
    }

    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    if (error.response.status === 401) {
      router.push('/login').then()
    }
    return Promise.reject(error)
  }
)

Vue.config.productionTip = false
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
