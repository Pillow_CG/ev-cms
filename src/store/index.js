import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

// 1、导入持久化存储 vuex 中数据的第三方包
import create from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({

  // 2. 配置为 vuex 的插件
  plugins: [create()],

  state: {
    // 先读取本地存储,如果没有,就设为空
    // token: localStorage.getItem('token') || ''  // 使用持久化存储插件后可删除
    token: '',

    // 2、vuex 调用actions函数initUserInfo，发送ajax，收集用户信息，
    // 2、调用mutations里的函数updateUserInfo储存进state
    userInfo: {}
  },

  getters: {
  },

  mutations: {
    newToken (state, newToken) {
      state.token = newToken
    },
    updateUserInfo (state, newUserInfo) {
      state.userInfo = newUserInfo
    }
  },

  actions: {
    // 请求用户信息函数
    async initUserInfo (context) {
      // 上面函数需加 async
      const { data: res } = await axios({
        url: '/my/userinfo',
        method: 'get'
        // headers: { Authorization: context.state.token }
      })
      // console.log(res)
      context.commit('updateUserInfo', res.data)
    }
  },

  modules: {
  }

})
