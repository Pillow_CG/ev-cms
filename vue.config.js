const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    open: true, // 自动打开浏览器
    port: 8888, // 端口号更改
    host: '127.0.0.1' // 设置端口号
  },
  lintOnSave: false // 检查你的代码风格
})
